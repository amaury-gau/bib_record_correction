import requests, colorama,re
from urllib.request import urlopen, Request
from urllib.parse import urlparse, urljoin 
from bs4 import BeautifulSoup

# initie le module colorama pour distinguer visuellement les liens
colorama.init()
GREEN = colorama.Fore.GREEN
GRAY = colorama.Fore.LIGHTBLACK_EX
RESET = colorama.Fore.RESET
YELLOW = colorama.Fore.YELLOW

# initie des ensemble de liens uniques
internal_urls = set()
external_urls = set()

def is_valid(url):
    """
    Vérifie si un lien internet est valide ou non. 
    """
    parsed = urlparse(url)
    return bool(parsed.netloc) and bool(parsed.scheme)

def all_links (url):
    """
    Retourne tous les URLs trouvés dans "url" et dans le site auquel il réfère 
    """
    # toutes les URLs de 'url'
    urls = set()
    # nom de domaine de l'URL sans son protocole
    domain_name = urlparse(url).netloc
    soup = BeautifulSoup(requests.get(url).content, "html.parser")

    # Création de tags pour chaque URl récupéré
    for a_tag in soup.find_all("a"):
        href = a_tag.attrs.get("href")
        if href == "" or href is None:
            # on vérifie s'il y a quelque chose dans l'URL. Sinon, on passe
            continue

        # On joint les URLS au cas où les dits URLs récupérés sont relatifs
        href = urljoin(url, href)

        parsed_href = urlparse(href)
        # On supprime les paramètres GET, fragments d'URL, etc pour évider les répétitions.
        href = parsed_href.scheme + "://" + parsed_href.netloc + parsed_href.path

        if not is_valid(href):
            # si URL invalide
            continue
        if href in internal_urls:
        # si déjà dans l'ensemble
            continue
        if domain_name not in href:
            # si lien extérieur
            if href not in external_urls:
                #print(f"{GRAY}[!] External link: {href}{RESET}")
                external_urls.add(href)
            continue
        #print(f"{GREEN}[*] Internal link: {href}{RESET}")
        urls.add(href)
        internal_urls.add(href)
    return urls

# On va récupérer le nombre de liens visités ici : 
total_urls_visited = 0

def crawl(url, max_urls=50):
    """
    Recherche tous les liens d'une page web et en extrait tous les liens.
    On trouvera tous les liens dans les variables `external_urls` et `internal_urls`.
    Paramètres : 
        max_urls (int) : nombre max d'urls à récupérer, 30 par défaut.  
    """
    #global permet de modifier une variable en dehors d'une fonction
    global total_urls_visited 
    global internal_urls 
    internal_urls = set()
    total_urls_visited += 1
    #print(f"{YELLOW}[*] Crawling: {url}{RESET}")
    links = all_links(url)
    
    for link in links:
        if total_urls_visited > max_urls:
            break
        crawl(link, max_urls=max_urls)
    
    return internal_urls

def  url_pattern (pattern, links) : 
    """
    Selects urls that are following a given pattern. 
    The patterns expected are the language code inscription in wikipedia urls. 
    """
    target_urls = set()

    patterns = re.compile(f'https?://)?{pattern}.wikipedia.org.*')

    for url in links : 
        if re.findall(patterns, url): 
            target_urls.add(url)
        else : 
            continue 
    
    return target_urls

##### web scraping  : 

def scrape_header(url):
    """
    Sends my informations to the websites so they know or don't ban my IP from 
    scraping datas.
    Parameter :
        url (str) : need an url
    """
    headers = {'User-Agent': 'Mozilla/5.0'}
    request = Request(url, headers=headers)

    return request

def open_url(url):
    """
    Opens the url and returns the method to process it.
    Parameter : 
        url (str) : need a url
    """
    link = scrape_header(url)
    html = urlopen(link) 

    return BeautifulSoup(html, 'html.parser')

def get_dump_text (url):
    """
    Gets all the raw text from a html file into a text file. 
    Parameter : 
        url (bs4.BeautifulSoup) : Needs the result of BeautifulSoup
    """

    soup = open_url(url)
    dump_text = soup.get_text()

    new_text = ''
    for line in dump_text.split('\n'): 
        new_text += line

    return new_text    
 
