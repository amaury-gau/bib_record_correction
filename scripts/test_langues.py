import sys
import polars as pl  

if __name__ == '__main__' : 
    
    df = pl.read_csv(sys.argv[1], separator='\t', truncate_ragged_lines=True)
    
    print(df.select(pl.col('*')))
    
    languages = df["langue"].to_list()
    scripting = df['écriture'].to_list()  

    
    print("nb langues : ", len(set(languages)))
    print("nb écritures : ", len(set(scripting)))

    print(languages.count("NULL"))
