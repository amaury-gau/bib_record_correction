import os, sys, re, math, numpy as np
from pathlib import Path
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer


def get_corpus(path : str) -> dict : 

    text = ''
    corpus = {}
    p = Path(path) 
    for language in p.iterdir() : 
        if language.is_dir() :
            for file in language.iterdir() : 
                if file.name.endswith(".txt") : 
                    f=open(file, 'r')
                    t = f.read().split()
                    t="".join(t)
                    f.close()
                    text += t + '\n'
                
            corpus.update({language.name:text})
                 
    return corpus

def div_corpus(corpus : dict) : 

    lang = list(corpus.keys())

if __name__ == '__main__' : 

    path = sys.argv[1]
    corpus= get_corpus(path)
    #print(corpus.values())
    print (corpus.keys())

