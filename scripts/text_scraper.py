#!usr/bin/python3

import scraping_pattern as sc
import sys, os, time, argparse

def arguments() :

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "path", help = "the directory the files will be created in." 
    )
    parser.add_argument(
        "url", help="url link to start scraping websites"
    )
    parser.add_argument(
        "-n", help="max number of websites scraped",
        default= 500, type=int
    )

    return parser.parse_args() 

def check_path() : 

    if os.path.exists(DIRECTORY): 
        pass 
    else : 
        os.makedirs(DIRECTORY) 

if __name__ == '__main__' : 

    args = arguments()
    DIRECTORY = args.path
    check_path()
    url_time = time.time()
    links = sc.crawl(args.url)
    print (f'{len(links)} links retrieved in {time.time() - url_time}s')
    #target_urls = sc.url_pattern(DIRECTORY.lower(), links)

    #print (len(target_urls))

    count = 0 
    scrape_time = time.time()
    for url in links : 
        if count < args.n : 
            try :
                text = sc.get_dump_text(url)
            except : 
                continue 

            count+=1
            f=open(f'{DIRECTORY}/{count}.txt', 'w', encoding='utf8')
            f.write (text)
            f.close()
        else : 
            break

    print (f'{count} texts scraped in {time.time() - scrape_time}s') 
