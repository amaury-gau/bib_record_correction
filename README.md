# Correction et translitteration de notices bibliographiques

_Cette page est en cours d'édition. Les informations inscrites ci-dessous seront très certainement modifiées au cours du projet_

## Genèse du projet

Ce projet a été motivé par une bibliothécaire chargée des collections ukrainiennes à la BULAC (Bibliothèque universitaire des langues et civilisations).
Du fait des historiques d'encodage et et catalographie, de nombreux exemplaires ukrainiens n'ont pas été encodés en caractères cyrillique, mais en caractères latins. Certains desdits exemplaires ont connu un ajout de langue originale en cyrillique russe.
D'autres ont bien été catalogués en cyrillique, mais avec un encodage russe, tandis qu'un encodage russe aurait été attendu. 
Une douzaine de milliers de notices snt concernés par ces scories et mériteraient d'être corrigées pour faire respecter les normes d'encodage. Le but serait de proposer un moyen de corriger ces notices automatiquement avec un modèle de langue. 

## Update du projet

Ce projet, désormais nommé **TransliTAL**, est lauréat de l'[appel à initiatives 2023 du programme de coopération](https://fil.abes.fr/2024/01/29/programme-de-cooperation-sept-projets-retenus/#:~:text=Le%2016%20novembre%20dernier%2C%20le,initiatives%20lanc%C3%A9%20en%20juillet%202023.) de l'Agence bibliographique de l'enseignement supérieur (Abes). Ce programme est abondé par des fonds du [GIS CollEx/Persée](https://www.collexpersee.eu/).  


## Principaux objectifs de ce projet

Corriger et (rétro)-translittérer les notices catalographiques de documents en écritures non-latines à l’aide de modèles de langues de
traitement automatique des langue (TAL). Cela permettra ainsi d’homogénéiser la qualité des notices bi-écriture standardisée
systématique - en s’appuyant par exemple sur la révision de la translittération du persan à l’aune de la norme ISO 233-3:2023(E) - et
de corriger les erreurs d’identification de langues.
Les différences ou erreurs de translittération qui peuvent se trouver dans les notices obèrent la qualité des données catalographiques
des documents et, par là même, dégradent le service rendu aux chercheurs et autres utilisateurs.
Il s’agit de répondre à deux besoins :
- distinguer et corriger les erreurs de transcription ou de translittération présentes dans les notices liées aux ambiguïtés d’identification
des langues concernées ;
- ajouter la translittération, ou rétro-translittération, quand elle manque à la notice, de manière automatique.
Nous envisageons d’appliquer l’outil sur plusieurs systèmes d’écritures et langues concernées par ces problématiques de qualité des
données catalographiques :
- écritures arabo-persanes : arabe, persan, urdu, turc
- cyrillique : russe, ukrainien, biélorusse, serbe, bulgare et autres langues cyrillisées (Asie centrale, Mongolie, ...)
- écritures à sinogrammes : chinois, japonais, coréen (CJK), viêtnamien chữ nôm, etc. 

## Données d'entraînement

Les données d'entraînement proviennent des notices bibliographiques propres au catalogue de la Bulac. Il n'est pas dit qu'on vienne les enrichir de quelques données du Sudoc. 
Les données sont sous formes de .css ou dataframe (pandas, polars) et contiennent les informations suivantes : 

> _Biblionumber, ppn, titre, auteur, maison d'édition, lieu d'édition, langue (ISO), écriture (ISO)_ 


>Les codes de langues ou d'écritures peuvent être multivalués, ce qui peut faire de nombreux n-uplets de langues. Il est aussi possible qu'un document soit entièrement écrit dans une langue A, mais un résumé a été traduit dans une langue B (avec même une écriture B), ce qui crée un couple de langues / écritures. 